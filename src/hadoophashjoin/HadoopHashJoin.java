package hadoophashjoin;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.GenericOptionsParser;

/**
 * This class contains the main function. The program must be run with the
 * following command:
 * hadoop jar exe.jar relation1 relation2 outputdir joinattribute numberOfReducers
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class HadoopHashJoin {

    public static final Log LOG = LogFactory.getLog(HadoopHashJoin.class);
    public static final boolean VERBOSE = false;

    public static boolean phase2(Configuration conf) {
        return false;
    }

    // id(dec number),string(string),salary(dec number),rating(float number)\n
    public static void main(String[] args) throws Exception {
        // FIXME: Open the file and read the 1st line to find the attributes.
        // FIXME: Check both files for join attribute existance.
        int joinAttr = 0;
        if (args[3].equals("id")) {
            joinAttr = 0;
        } else if (args[3].equals("string")) {
            joinAttr = 1;
        } else if (args[3].equals("salary")) {
            joinAttr = 2;
        } else if (args[3].equals("rating")) {
            joinAttr = 3;
        } else {
            throw new Exception("Join attribute does not exists");
        }

        Configuration conf = new Configuration();
        conf.set("tuc.hadoop.hashjoin.relation1", args[0]);
        conf.set("tuc.hadoop.hashjoin.relation2", args[1]);
        conf.set("tuc.hadoop.hashjoin.outputDir", args[2]);
        conf.set("tuc.hadoop.hashjoin.joinAttribute", Integer.toString(joinAttr));
        conf.set("tuc.hadoop.hashjoin.numberOfReducers", args[4]);


        LOG.info("================= INFO =================");
        LOG.info("relation1: " + conf.get("tuc.hadoop.hashjoin.relation1"));
        LOG.info("relation2: " + conf.get("tuc.hadoop.hashjoin.relation2"));
        LOG.info("outputDir: " + conf.get("tuc.hadoop.hashjoin.outputDir"));
        LOG.info("joinAttribute: " + conf.get("tuc.hadoop.hashjoin.joinAttribute"));
        LOG.info("numberOfReducers: " + conf.get("tuc.hadoop.hashjoin.numberOfReducers"));
        LOG.info("========================================");

        // Silences a logging warning.
        GenericOptionsParser otherArgs = new GenericOptionsParser(conf, args);

        boolean isCompleted = false;

        LOG.info("============ PARTITION PHASE ===========");
        PartitionPhase phase1 = new PartitionPhase(conf);
        isCompleted = phase1.execute(VERBOSE);
        if (!isCompleted) {
            LOG.fatal("\t--> Partition Phase Failure!");
            System.exit(1);
        }
        LOG.info("\t--> Partition Phase Completed!");

        LOG.info("============== PROBE PHASE =============");
        ProbePhase phase2 = new ProbePhase(conf);
        isCompleted = phase2.execute(VERBOSE);
        if (!isCompleted) {
            LOG.fatal("\t--> Probe Phase Failure!");
            System.exit(1);
        }
        LOG.info("\t--> Probe Phase Completed!");

        LOG.info("======= GRACE HASH JOIN COMPLETED ======");
    }
}