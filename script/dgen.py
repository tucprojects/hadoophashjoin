from random import *
from math import log

class Generator:
    def __init__(self):
        self.gen = Random()
        self.gen.seed()
    def getDistribution(self, type, size, params):
        result = []
        if type == 'beta':
            #gen beta
            alpha = params[0]
            beta = params[1]
            for i in range(0, size):
                result.append(self.gen.betavariate(alpha, beta))
           # return result;
        elif type == 'exp':
            #gen beta
            lampd = params[0]
            if lampd < 0 :
                lampd *= -1
            for i in range(0, size):
                result.append(self.gen.expovariate(lampd))
            #return result;
        elif type == 'gamma':
            #gen beta
            alpha = params[0]
            beta = params[1]
            for i in range(0, size):
                result.append(self.gen.gammavariate(alpha, beta))
            #return result;
        elif type == 'gauss':
            #gen beta
            mu = params[0]
            sigma = params[1]
            for i in range(0, size):
                result.append(self.gen.gauss(mu, sigma))
            #return result;
        elif type == 'pareto':
            #gen beta
            alpha = params[0]
            #beta = params[1]
            for i in range(0, size):
                result.append(self.gen.paretovariate(alpha))
            #return result;
        elif type == 'weibull':
            #gen beta
            alpha = params[0]
            beta = params[1]
            for i in range(0, size):
                result.append(self.gen.weibullvariate(alpha, beta))
            #return result;
        elif type == 'uniform':
            #gen beta
            alpha = params[0]
            beta = params[1]
            for i in range(0, size):
                result.append(self.gen.uniform(alpha, beta))
            #return result;
        #for indx in range(0,len(result)):
            #result[indx] =log(result[indx])
        return result    
    
    def getLogDistribution(self, type, size, params):
        result = []
        if type == 'beta':
            #gen beta
            alpha = params[0]
            beta = params[1]
            for i in range(0, size):
                result.append(log(self.gen.betavariate(alpha, beta)))
           # return result;
        elif type == 'exp':
            #gen beta
            lampd = params[0]
            if lampd < 0 :
                lampd *= -1
            for i in range(0, size):
                result.append(log(self.gen.expovariate(lampd)))
            #return result;
        elif type == 'gamma':
            #gen beta
            alpha = params[0]
            beta = params[1]
            for i in range(0, size):
                result.append(log(self.gen.gammavariate(alpha, beta)))
            #return result;
        elif type == 'gauss':
            #gen beta
            mu = params[0]
            sigma = params[1]
            for i in range(0, size):
                result.append(log(self.gen.gauss(mu, sigma)))
            #return result;
        elif type == 'pareto':
            #gen beta
            alpha = params[0]
            #beta = params[1]
            for i in range(0, size):
                result.append(log(self.gen.paretovariate(alpha)))
            #return result;
        elif type == 'weibull':
            #gen beta
            alpha = params[0]
            beta = params[1]
            for i in range(0, size):
                result.append(log(self.gen.weibullvariate(alpha, beta)))
            #return result;
        elif type == 'uniform':
            #gen beta
            alpha = params[0]
            beta = params[1]
            for i in range(0, size):
                result.append(log(self.gen.uniform(alpha, beta)))
            #return result;
        #for indx in range(0,len(result)):
            #result[indx] =log(result[indx])
        return result
            
    def getNumber(self, low, high):
        if low >= high:
            return low
        
        
        result = self.gen.randint(low, high)
        return result;
    
    def choose(self, seq):
        return self.gen.choice(seq)
