% Multiple mappers in partition phase

part1 = [21 22 23 25 25 27 28 29 31 32 33 35 36 38 38 39 41 43 43 45 46];
part2 = part1 * 2;
probe = [174 114 134 202 125 146 103 94 207 143 96 92 194 405 117 96 132 94 112 138 114];
total = probe + part2;
x = [1 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100];

plot(x, total, x, part2, x, probe);
legend('Total','Partition Phase','Probe Phase');
grid on;
xlabel('Reducer number');
ylabel('Time (sec)');