package hadoophashjoin;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * ProbePhase is the second and last phase of the GRACE hash join algorithm.
 * Before it gets started it requires that both relation are partitioned from
 * phase 1 at {@link PartitionPhase}. It creates 1 job that executes the join
 * between the relations and the merge into 1 file. The input of the job is the
 * R relation. Then, each mapper is responsible to build an in memory hash-table
 * of the S relation. Actually a part of it, base on the input split
 * (see {@see HashProbeMapper} for details).
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class ProbePhase {

    public static final Log LOG = LogFactory.getLog(PartitionPhase.class);
    private Configuration conf_;
    private Job jobProbe_;

    /**
     * Constructor.
     *
     * @param conf the configuration to be used for the jobs.
     */
    public ProbePhase(Configuration conf) {
        conf_ = conf;
    }

    /**
     * This method creates a Job object. Note that the properties
     * "tuc.hadoop.hashjoin.numberOfReducers" and
     * "tuc.hadoop.hashjoin.outputDir" must be set before calling this method.
     *
     * @param jobName of the job to be created.
     * @param relationFile that will be used as the input of the job.
     * @param out directory where the resulted files will be written.
     * @return a Job object.
     * @throws IOException if the provided relationFile doesn't exist.
     */
    private Job createJob(String jobName, String relationFile, String out)
            throws IOException {
        Job job = new Job(conf_, jobName);
        job.setJarByClass(HadoopHashJoin.class);

        job.setMapperClass(HashProbeMapper.class);
        job.setReducerClass(HashReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setNumReduceTasks(1);

        FileInputFormat.addInputPath(job, new Path(relationFile));
        FileOutputFormat.setOutputPath(job,
                new Path(conf_.get("tuc.hadoop.hashjoin.outputDir") + "/" + out));

        return job;
    }

    /**
     * It creates 1 job that makes the join and merges into 1 file that final
     * output.
     *
     * @param verbose true for a detailed output
     * @return true if the jobs where successful.
     * @throws IOException
     * @throws InterruptedException
     * @throws ClassNotFoundException
     */
    public boolean execute(boolean verbose)
            throws IOException, InterruptedException, ClassNotFoundException {
        jobProbe_ = createJob("hashjoin_probe",
                conf_.get("tuc.hadoop.hashjoin.part.R.out"), "probe-out");

        jobProbe_.waitForCompletion(verbose);

        LOG.info("Probe successful: " + jobProbe_.isSuccessful());
        LOG.info("Probe ouput: " + FileOutputFormat.getOutputPath(jobProbe_));
        conf_.set("tuc.hadoop.hashjoin.probe.out",
                FileOutputFormat.getOutputPath(jobProbe_).toString());

        return jobProbe_.isSuccessful();
    }
}
