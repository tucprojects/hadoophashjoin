import os
import sys
from dgen import Generator
from random import shuffle

def generateString(length):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    result =""
    for i in range(0,length):
        result += gen.choose(alphabet)
    return result;

R = ""
S = ""
count = 0

if len(sys.argv) == 4:
    R = sys.argv[1]
    S = sys.argv[2]
    count = int(sys.argv[3])
    

Rfile = open(R,"w")
# Rfile.write("id | string | salary | rating\n")
Sfile = open(S+"tmp","w")
gen = Generator()

joins = 0
progress = 0.0
for index in range(0,count):
    if progress - index/float(count) < -0.1:
        progress = index/float(count)
        print 'progress: ',progress
    id = index
    string = generateString(gen.getNumber(3, 6))
    salary = gen.getNumber(1000, 1500)
    d = gen.getDistribution('gauss', 1, [1000,10])[0]
    sstring = ""
    ssalary = int(0)
    sd = 0.0
    joinProbability = gen.getDistribution('gauss', 1, [0,1])[0]
    if joinProbability >= -0.25:
        jattr = gen.getNumber(0, 2)
        joins +=1
        if jattr == 0:
            sstring = string
            ssalary = gen.getNumber(1000,1500)
            sd = gen.getDistribution('gauss',1,[1000,10])[0]
        elif jattr == 1:
            ssalary = salary
            sstring = generateString(gen.getNumber(3, 6))
            sd = gen.getDistribution('gauss', 1, [1000,10])[0]
        else:
            sd = d
            sstring = generateString(gen.getNumber(3, 6))
            ssalary = gen.getNumber(1000, 1500)
    else:
        sstring = generateString(gen.getNumber(3, 6))
        sd = gen.getDistribution('gauss', 1, [950,50])[0]
        ssalary = gen.getNumber(1350, 1780)
        
    strR = str(id)+","+string+","+str(salary)+","+str(d)
    strS =  sstring+","+str(ssalary)+","+str(sd)
    Rfile.write(strR + '\n')
    Sfile.write(strS + '\n')
Rfile.close()
Sfile.close()
Sfile = open(S,"w")
tmpSfile = open(S+"tmp","r")
lines = tmpSfile.readlines()
for i in range(0,3):
    shuffle(lines)

indx = 0
# Sfile.write("id | string | salary | rating\n")
for line in lines:
    Sfile.write(str(indx)+","+line)
    indx +=1
    
    
Sfile.close()
tmpSfile.close()
os.system("rm "+S+"tmp")
print str(count) , " rows are generated which at least",joins , ' are joinable'    
    
            
    
