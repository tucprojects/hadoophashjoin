package hadoophashjoin;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * A simple reducer that just writes in a file what it gets from the mappers. It
 * is used during both phases of the algorithm.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class HashReducer extends Reducer<Text, Text, Text, Text> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {
        for (Text text : values) {
            context.write(key, text);
        }
    }
}
