'''
Created on May 7, 2011

@author: vagvaz
'''
from itertools import chain
from itertools import combinations

def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    c =  chain.from_iterable(combinations(s, r) for r in range(len(s)+1))
    tmp = c.next()
    #tmp = c.next()
    result = []
    try:
        while tmp != None:
            tmp = c.next()
            result.append(tmp)
            
    except:
        pass
        #end of iteration
    return result
        

def getDivisors(number):
    result = []
    tmp = number
    curDivisor = 2
    while tmp != 1:
        if tmp % curDivisor == 0:
            tmp /= curDivisor
            result.append(curDivisor)
        else:
            curDivisor += 1
    #print result,number
    return result

def isprime(number):
    divisors = getDivisors(number)
    if len(divisors) == 1:
        return True,divisors
    else:
        return False,divisors
    