package hadoophashjoin;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * This mapper is used in the first phase of the algorithm. It takes a relation
 * and transmits a key - value pair. The key is a hash of the relation
 * attribute used for the join and the value is the complete tuple. A hash is
 * produced based on the number of the reducers. As a consequence the final
 * number of buckets produced is equal to the number of reducers.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class HashPartitionMapper extends Mapper<Object, Text, Text, Text> {

    public static final Log LOG = LogFactory.getLog(HashPartitionMapper.class);
    private static final String DELIMETER = ",";
    private int JOIN_ATTRIBUTE;
    private String REDUCER_NUM;
    private Text transmitKey_ = new Text();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Configuration conf = context.getConfiguration();
        JOIN_ATTRIBUTE = Integer.parseInt(
                conf.get("tuc.hadoop.hashjoin.joinAttribute"));
        REDUCER_NUM = conf.get("tuc.hadoop.hashjoin.numberOfReducers");
    }

    /**
     * Given a text and an algorithm, this method produces a hash that will be
     * transmitted to the reducers as a key. After the message digest gets
     * produced, it gets mod with the number of reducers.
     *
     * @param text from where the hash will be produced.
     * @param algorithm that will be used.
     * @return a string representation of the hash in decimal form.
     * @throws NoSuchAlgorithmException
     */
    public String keyHash(String text, String algorithm)
            throws NoSuchAlgorithmException {
        byte[] hash = MessageDigest.getInstance(algorithm).digest(text.getBytes());
        BigInteger bi = new BigInteger(1, hash);
        bi = bi.mod(new BigInteger(REDUCER_NUM));
        String result = bi.toString(10);
        if (result.length() % 2 != 0) {
            return "0" + result;
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void map(Object key, Text value, Context context)
            throws IOException, InterruptedException {
        String[] valueSplit = value.toString().split(DELIMETER);

        String hash = null;
        try {
            hash = keyHash(valueSplit[JOIN_ATTRIBUTE], "SHA-1");
        } catch (NoSuchAlgorithmException ex) {
            LOG.fatal(ex);
        } catch (ArrayIndexOutOfBoundsException ex) {
            LOG.fatal(ex);
        }

        transmitKey_.set(hash);
        context.write(transmitKey_, value);
    }
}
