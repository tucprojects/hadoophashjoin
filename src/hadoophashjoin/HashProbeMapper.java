package hadoophashjoin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

/**
 * This mapper is used in the second phase of the algorithm and after the two
 * relations are partitioned into buckets. The relation R is the input of this
 * mapper. Before it starts receiving keys-values pairs from R, during the setup
 * it builds an in memory hash-table from the S relation.
 *
 * It finds out the file name(s) where the InputSplit will come from. This is
 * in the directory of the R relation produced during phase 1. In the directory
 * of S relation there will be files named exactly the same, and so the mapper
 * opens and reads from them to build the hash-table.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class HashProbeMapper extends Mapper<Object, Text, Text, Text> {

    public static final Log LOG = LogFactory.getLog(HashProbeMapper.class);
    private static final String DELIMETER = ",";
    private static final String RELATION_2 = "S";
    private int JOIN_ATTRIBUTE;
    private Text transmitKey_ = new Text();
    private Map<String, List<String>> relationHash_;

    /**
     * When this method is called, it is assumed that the properies
     * "tuc.hadoop.hashjoin.joinAttribute", "tuc.hadoop.hashjoin.outputDir" are
     * both set. Also there must be a directory for each relation containing
     * the file parts (part-r-*) produced from phase 1.
     * 
     * {@inheritDoc}
     */
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        relationHash_ = new HashMap<String, List<String>>();


        Configuration conf = context.getConfiguration();
        JOIN_ATTRIBUTE = Integer.parseInt(
                conf.get("tuc.hadoop.hashjoin.joinAttribute"));

        FileSplit split = (FileSplit) context.getInputSplit();
        String splitFilename = split.getPath().getName();
        Path rel2Path = new Path(conf.get("tuc.hadoop.hashjoin.outputDir")
                + "/" + RELATION_2 + "/" + splitFilename);
        FileSystem fs = FileSystem.get(conf);
        BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(rel2Path)));

        String line = br.readLine();
        while (line != null) {
            String[] lineSplit = line.split("\t");
            String[] tupleSplit = lineSplit[1].split(DELIMETER);
            try {
                String keyHash = keyHash(tupleSplit[JOIN_ATTRIBUTE], "SHA-1");
                if (!relationHash_.containsKey(keyHash)) {
                    List<String> list = new ArrayList<String>();
                    list.add(lineSplit[1]);
                    relationHash_.put(keyHash, list);
                } else {
                    relationHash_.get(keyHash).add(lineSplit[1]);
                }
            } catch (NoSuchAlgorithmException ex) {
                LOG.fatal(ex);
            }
            line = br.readLine();
        }
        //System.out.println("File name " + splitPath.getName());
        //System.out.println("Directory and File name"+split.getPath().toString());
    }

    /**
     * Given a text and an algorithm, this method produces a hash that will be
     * transmitted to the reducers as a key.
     *
     * @param text from where the hash will be produced.
     * @param algorithm that will be used.
     * @return a string representation of the hash in decimal form.
     * @throws NoSuchAlgorithmException
     */
    public String keyHash(String text, String algorithm)
            throws NoSuchAlgorithmException {
        byte[] hash = MessageDigest.getInstance(algorithm).digest(text.getBytes());
        BigInteger bi = new BigInteger(1, hash);
        String result = bi.toString(10);
        if (result.length() % 2 != 0) {
            return "0" + result;
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void map(Object key, Text value, Context context)
            throws IOException, InterruptedException {
        String[] keyValue = value.toString().split("\t");
        String[] tupleSplit = keyValue[1].split(DELIMETER);


        String hash = null;
        try {
            hash = keyHash(tupleSplit[JOIN_ATTRIBUTE], "SHA-1");
        } catch (NoSuchAlgorithmException ex) {
            LOG.fatal(ex);
        } catch (ArrayIndexOutOfBoundsException ex) {
            LOG.fatal(ex);
        }

        if (relationHash_.containsKey(hash)) {
            //String rel2value = relationHash_.get(hash);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < tupleSplit.length; i++) {
                builder.append(tupleSplit[i]);
                if (i != tupleSplit.length - 1) {
                    builder.append(",");
                }
            }

            Collection<String> rel2Values = relationHash_.get(hash);
            for (String val : rel2Values) {
                transmitKey_.set(val);
                context.write(transmitKey_, new Text(builder.toString()));
            }
        }
    }
}
