package hadoophashjoin;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * PartitionPhase is the first phase of the GRACE hash join algorithm. Given 2
 * relations R and S, it creates 2 jobs that partition both of them. The file
 * parts that will be created for the relations in separate directories
 * represent the buckets of the algorithm. This number is defined by the number
 * of the reducer to be used. So in the first phase, that hash function that is
 * used, is (key.hash() % #reducer).
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class PartitionPhase {

    public static final Log LOG = LogFactory.getLog(PartitionPhase.class);
    private Configuration conf_;
    private Job jobR_;
    private Job jobS_;

    /**
     * Constructor.
     *
     * @param conf the configuration to be used for the jobs.
     */
    public PartitionPhase(Configuration conf) {
        conf_ = conf;
    }

    /**
     * This method creates a Job object. Note that the properties
     * "tuc.hadoop.hashjoin.numberOfReducers" and
     * "tuc.hadoop.hashjoin.outputDir" must be set before calling this method.
     *
     * @param jobName of the job to be created.
     * @param relationFile that will be used as the input of the job.
     * @param out directory where the resulted files will be written.
     * @return a Job object.
     * @throws IOException if the provided relationFile doesn't exist.
     */
    private Job createJob(String jobName, String relationFile, String out)
            throws IOException {
        Job job = new Job(conf_, jobName);
        job.setJarByClass(HadoopHashJoin.class);

        job.setMapperClass(HashPartitionMapper.class);
        job.setReducerClass(HashReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setNumReduceTasks(Integer.parseInt(
                conf_.get("tuc.hadoop.hashjoin.numberOfReducers")));

        Path path = new Path(relationFile);
        FileSystem fileSystem = path.getFileSystem(conf_);
        FileStatus[] listStatus = fileSystem.listStatus(path);

        /* To start with multiple mappers and not only 1 map task, I change the
         * max split size of input. The number of mappers will be equal to the
         number of reducers. */
        long maxSplit = listStatus[listStatus.length-1].getLen() /
                Long.parseLong(conf_.get("tuc.hadoop.hashjoin.numberOfReducers"));

        FileInputFormat.setMaxInputSplitSize(job, maxSplit);
        FileInputFormat.addInputPath(job, new Path(relationFile));


        FileOutputFormat.setOutputPath(job,
                new Path(conf_.get("tuc.hadoop.hashjoin.outputDir") + "/" + out));

        return job;
    }

    /**
     * It creates 1 job for each relation and then they get executed.
     *
     * @param verbose true for a detailed output
     * @return true if the jobs where successful.
     * @throws IOException
     * @throws InterruptedException
     * @throws ClassNotFoundException
     */
    public boolean execute(boolean verbose)
            throws IOException, InterruptedException, ClassNotFoundException {
        jobR_ = createJob("hashjoin_partition_R",
                conf_.get("tuc.hadoop.hashjoin.relation1"), "R");
        jobS_ = createJob("hashjoin_partition_S",
                conf_.get("tuc.hadoop.hashjoin.relation2"), "S");



        LOG.info("========================================");
        LOG.info("block size: " + conf_.get("dfs.block.size"));
        LOG.info("Min split size: " + conf_.get("mapred.min.split.size"));
        LOG.info("Max split size: " + conf_.get("mapred.max.split.size"));
        LOG.info("========================================");

//        jobR_.submit();
//        jobS_.submit();
//        while (!jobR_.isComplete()) {
//        }
//        while (!jobS_.isComplete()) {
//        }

        jobR_.waitForCompletion(verbose);
        jobS_.waitForCompletion(verbose);

        LOG.info("R successful: " + jobR_.isSuccessful());
        LOG.info("S successful: " + jobR_.isSuccessful());
        LOG.info("R ouput: " + FileOutputFormat.getOutputPath(jobR_));
        LOG.info("S ouput: " + FileOutputFormat.getOutputPath(jobS_));
        conf_.set("tuc.hadoop.hashjoin.part.R.out", FileOutputFormat.getOutputPath(jobR_).toString());
        conf_.set("tuc.hadoop.hashjoin.part.S.out", FileOutputFormat.getOutputPath(jobS_).toString());

        return (jobR_.isSuccessful() && jobS_.isSuccessful());
    }
}
